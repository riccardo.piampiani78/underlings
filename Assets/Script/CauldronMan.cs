    using System.Collections;
    using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Xml.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UI;

public enum ERecipeState
{
    Perfect,
    MissingOptional,
    Failed,
    OutOfElements
}
public class CauldronMan : MonoBehaviour
    {   
        public Collider2D couldronCollider;
        public Transform player;
        public InventoryMan _inventoryman;
        List<string> _itemsInCauldron = new List<string>();
        public List<string> _itemsToCheck = new List<string>();
        public List<string> _itemsWithTagContainer = new List<string>();
        public List<Cookbook> Recipes = new List<Cookbook>();
        private Cookbook RecipeInUse;
        public bool OkMust = false;
        public bool OkBonus = false; 
        public bool RecipeReady = false;
        public string Outcome = "";
    public void Awake()
    {
        RecipeInUse = Recipes.FirstOrDefault();
        GameObject[] allObjects = FindObjectsOfType<GameObject>();
        foreach (GameObject obj in allObjects)
        {
            if (obj.GetComponent<SpriteRenderer>() != null) {
                if (obj.tag != "Container" )
                    _itemsToCheck.Add(obj.GetComponent<SpriteRenderer>().sprite.name);
                else
                    _itemsWithTagContainer.Add(obj.GetComponent<SpriteRenderer>().sprite.name);
            }

        }
        
        _itemsToCheck = _itemsToCheck.Distinct().ToList();
    }


    public void toggleCollider(Transform playerCoordinate)

        {
                if (playerCoordinate.position.y < -1.50)
            {
              //  couldronCollider.enabled = false;
                GetComponent<SpriteRenderer>().sortingOrder = 2;
            }

            else
            {
                GetComponent<SpriteRenderer>().sortingOrder = 4;
            }   

        }
        void Update()
        {
            toggleCollider(player);
 
        }
        public void Interact()
        {
        if (_inventoryman.useTool.sprite.name == "mestolopocket")
        {
            RecipeReady = true;
            CheckRecipes();
            
            return;

        }

        if (_inventoryman.useTool.sprite.name == "containerpoket")
        {
            if (RecipeReady)
                _itemsInCauldron.Clear();
            //va messo in container diverso, magari lo stesso container con del fumo che esce dalla
            //tazza o pentola che sia
        }

            string ItemInUse = _inventoryman.useTool.sprite.name.Replace("pocket", "");
        if (_itemsToCheck.Contains(ItemInUse))
            {

                _itemsInCauldron.Add(ItemInUse);
            _inventoryman.deleteTool();

            }

            else
        {
                _itemsInCauldron.Clear();
            //qua va aggiunto cosa deve mettere nel container dopo aver interagito
        }
        }
    
    private void CheckRecipes()
    {
        List<string> oggettinecessari = RecipeInUse.MustItem.Select(x => x.Name).ToList();
        if (RecipeInUse.ItemMaxNumber< _itemsInCauldron.Count())
            Outcome= ERecipeState.OutOfElements.ToString();

        if (oggettinecessari.All(element => _itemsInCauldron.Contains(element)))

        {
            List<string> oggettibonus = RecipeInUse.BonusItem.Select(x => x.Name).ToList();
            if (oggettibonus.All(element => _itemsInCauldron.Contains(element)))
            {
                if (RecipeInUse.ItemMaxNumber == _itemsInCauldron.Count())
                    Outcome = ERecipeState.Perfect.ToString();
            }
            Outcome = ERecipeState.MissingOptional.ToString();

        }
        // OkBonus = RecipeInUse.MustItem.All

        else
            Outcome = ERecipeState.Failed.ToString();
    }
    }
