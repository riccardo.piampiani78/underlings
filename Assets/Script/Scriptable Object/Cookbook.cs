﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "Cookbook", menuName = "Underling/Cookbook")]
public class Cookbook : ScriptableObject
    {
    public string Name;
    public string Description;
    public List<CollectableItem> MustItem = new List<CollectableItem>();
    public List<CollectableItem> BonusItem = new List<CollectableItem>();
    public string BonusMissing;
    public int ItemMaxNumber;  //numero massimo di elementi entro il quale la ricetta e' fallita

}
