﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Script.Scriptable_Object
{
    [CreateAssetMenu(fileName = "RoomLimiter", menuName = "Underling/RoomLimiter")]
    public class RoomLimiter : ScriptableObject
    {
        public float xMax;
        public float yMax;
        public float yMin;      
        public float xMin;
        public int RoomNumber;
        public Vector3 PlayerPosition;
    }
}
