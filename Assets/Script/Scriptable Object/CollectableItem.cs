﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName ="CollectableItem", menuName = "Underling/CollectableItem")]
public class CollectableItem : ScriptableObject
    {
    public string Name;
    public Image Image;


    }

