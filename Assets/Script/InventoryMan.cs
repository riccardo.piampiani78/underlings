                using System;
                using System.Collections;
                using System.Collections.Generic;
                using Unity.Collections.LowLevel.Unsafe;
                using UnityEngine;
                using UnityEngine.InputSystem;
                using UnityEngine.InputSystem.Android;
                using UnityEngine.UI;

public class InventoryMan : MonoBehaviour
{

    public int counter = 0;
    public Image image1;
    public Image image2;
    public Image image3;
    public Camera cam;
    public Transform player;
    public Transform container;
    public Transform judgmentMessage;
    public Image useTool;
    public int ToolInUse = 0;
    public Transform elements;

    public bool IsNotFull => counter < 3;
    private void Update()
    {
        container.position = player.position;
        judgmentMessage.position = player.position;

    }

    public void SwitchInventory(InputAction.CallbackContext keys)
    {
        if (keys.performed == false)
            return;

        if (counter == 0)
            return;

        changeItem();


    }


    public void getTool(string name)
    {

        if (IsNotFull)
        {
            switch (counter)
            {
                case 0:
                    image1.sprite = Resources.Load<Sprite>(name + "pocket");
                    useTool.sprite = image1.sprite;

                    break;
                case 1:
                    image2.sprite = Resources.Load<Sprite>(name + "pocket");
                    break;
                case 2:
                    image3.sprite = Resources.Load<Sprite>(name + "pocket");
                    break;
            }
            counter++;

        }
    }
    private void changeItem()
    {
        ToolInUse++;
        if (ToolInUse >= counter)
        {
            ToolInUse = 0;

        }
        switch (ToolInUse)
        {
            case 0:
                useTool.sprite = image1.sprite;

                break;
            case 1:
                useTool.sprite = image2.sprite;
                break;
            case 2:
                useTool.sprite = image3.sprite;
                break;
        }
    }
    public void DropTool(bool delete = false)
    {

        if (counter != 0)
        {

            string ImageName = useTool.sprite.name.Replace("pocket", "");
            int childs = elements.childCount;
            for (int i = 0; i < childs; i++)
            {
                GameObject child = elements.GetChild(i).gameObject;

                if (child.activeSelf == false && child.name == ImageName)
                {
                    if (delete != true)
                    {
                        child.SetActive(true);
                        child.transform.position = player.position;
                        break;
                    }
                    else
                    {
                        GameObject.Destroy(child);
                        break;
                    }


                }
            }

                if (counter == 3 && useTool.sprite == image2.sprite)
                    image2.sprite = image3.sprite;

                else if (counter == 3 && useTool.sprite == image1.sprite)
                {
                    image1.sprite = image2.sprite;
                    image2.sprite = image3.sprite;

                }

                else if (counter == 2 && useTool.sprite == image2.sprite)
                {

                    image2.sprite = Resources.Load<Sprite>("drawer1");
                }



                else if (counter == 2 && useTool.sprite == image1.sprite)
                {
                    image1.sprite = image2.sprite;
                    image2.sprite = Resources.Load<Sprite>("drawer1");

                }
                else if (counter == 1)
                {

                    image1.sprite = Resources.Load<Sprite>("drawer1");

                }


                image3.sprite = Resources.Load<Sprite>("drawer1");
                
                useTool.sprite = Resources.Load<Sprite>("drawer1");
                counter--;
                changeItem();
               
            
        }

    }

    public void deleteTool()
    {
        DropTool(true);
    }
}




