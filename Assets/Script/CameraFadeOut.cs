using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CameraFadeOut : MonoBehaviour
{
    public float fadeOutDuration = 2.0f; // Durata dell'effetto fade out in secondi

    private Image panelImage;
    private Color originalColor;
    private float timeElapsed = 0.0f;
    public bool ChangeScene = false;
    public bool FadingOut = true;
    public RoomsMan Rooms;
    public int roomNumber;
    private void Start()
    {
        panelImage = GetComponent<Image>();
        originalColor = panelImage.color;
        panelImage.color = new Color(originalColor.r, originalColor.g, originalColor.b, 0); // Imposta l'alpha del colore a 0 per renderlo trasparente all'inizio
    }

    public void CamFadeOut()
    {
        if (timeElapsed < fadeOutDuration)
        {
            timeElapsed += Time.deltaTime;
            float alpha = Mathf.Lerp(0, 1, timeElapsed / fadeOutDuration); // Calcola l'alpha desiderato con lerp
            panelImage.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha); // Imposta il colore con l'alpha calcolato
        }
    }

    public void CamFadeIn()
    {
        timeElapsed += Time.deltaTime;
        float alpha = Mathf.Lerp(1, 0, timeElapsed / fadeOutDuration); // Calcola l'alpha desiderato con lerp
        panelImage.color = new Color(originalColor.r, originalColor.g, originalColor.b, alpha); // Imposta il colore con l'alpha calcolato
    }


    public void Update()
    {
        if (ChangeScene)
        {
            if (timeElapsed >= 2) {
                timeElapsed = 0;
                FadingOut = false;
            }

            if (FadingOut)
                CamFadeOut();

            else
            {
                CamFadeIn();
                Rooms.setRoomNumber(roomNumber);
            }
            if (timeElapsed >= 2 && FadingOut == false)
                ChangeScene = false;


            }
            
        }


        }
    

