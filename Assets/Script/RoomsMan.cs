using Assets.Script.Scriptable_Object;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Threading;

public class RoomsMan : MonoBehaviour
{
    
    public List<RoomLimiter> Rooms = new List<RoomLimiter>();
    public CameraMan CameraMan;
    public Transform player;

    private void setRoom(RoomLimiter room)
    {
        CameraMan.setActualRoom(room);
        player.position = room.PlayerPosition;

    }

    public void setRoomNumber(int roomNumber)
    {
                RoomLimiter selectedRoom = Rooms.Where(x => x.RoomNumber == roomNumber).FirstOrDefault();
                setRoom(selectedRoom);
    }


    public void Awake()
    {
        setRoom(Rooms.First());
        
    }
}
