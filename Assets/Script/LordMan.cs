using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class LordMan : MonoBehaviour
{
    public CauldronMan CauldronMan;
    public FadeInSprite FadeInSprite;
    public void Judgment()
    {
        if (CauldronMan.Outcome == "")
        {
            FadeInSprite.SetText("Qua non c'e' niente! Muoviti a prepararmi quello che ti ho chiesto!!");
        }
        if (CauldronMan.Outcome == "Perfect")
        {
            FadeInSprite.SetText("Complimenti *underling* e' perfetto!");
        }

        if (CauldronMan.Outcome == "MissingOptional")
        {
            FadeInSprite.SetText("Bene! Ma sento che manca qualcosa!");
        }

        if(CauldronMan.Outcome == "Failed")
        {
            FadeInSprite.SetText("Bleah! E' orripilante!");
        }

        if (CauldronMan.Outcome == "OutOfElements")
        {
            FadeInSprite.SetText("Cos'e' questo intruglio?! C'e' troppa roba qua dentro!");
        }

        CauldronMan.Outcome = "";
        //va risettata l'immagine del container senza fumo
    }  

}
