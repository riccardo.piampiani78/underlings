using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerMan : MonoBehaviour
{
    public Rigidbody2D rb;
    public Animator animator;
    public float k;
    public GameObject Inventory;
    public InventoryMan inventoryMan;
    public Vector2 direction;
    public GameObject currentCollision;
    public CauldronMan CauldronMan;
    public LordMan LordMan;
    public CameraFadeOut CameraFadeOut;
    

    public void move(InputAction.CallbackContext keys)
    {
        direction = keys.ReadValue<Vector2>();
        // Debug.Log($"Direzione => {direction}");
        setVelocity();
        setdirection();
    }

    public void setVelocity()
    {
        animator.SetFloat("Velocity", direction.sqrMagnitude);
        rb.velocity = direction * k;
    }
    public void setdirection()
    {

        animator.SetFloat("X", direction.x);
        animator.SetFloat("Y", direction.y);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        currentCollision = collision.gameObject;

    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        currentCollision = null;
    }

    public void Interact(InputAction.CallbackContext keys)
    {
        if (keys.performed == false)
        return;
        if (currentCollision == null)
            return;


        if (currentCollision.tag == "Door")
        {
            CameraFadeOut.ChangeScene = true;
            CameraFadeOut.roomNumber = currentCollision.GetComponent<DoorMan>().RoomToOpen;
            
            currentCollision.GetComponent<Interations>().OpenDoor();
            
            
            return;
        }

        if (currentCollision.tag == "Tool")
        {
            currentCollision.GetComponent<ToolMan>().GetTool();

            return;


            

        }
        if (currentCollision.tag == "Cauldron")
        {
            currentCollision.GetComponent<CauldronMan>().Interact();

            return;




        }

        if (currentCollision.tag == "Lever")
        {
            if (CauldronMan._itemsWithTagContainer.Contains(inventoryMan.useTool.sprite.name.Replace("pocket", "")))
            {
                LordMan.Judgment();
            }
            //currentCollision.GetComponent<LordMan>().Judgment();

            return;




        }

        if (currentCollision.tag == "Container")
        {
            currentCollision.GetComponent<ToolMan>().GetTool();

            return;



        }

      /*  if (currentCollision.tag == "Limiter")
        {
            currentCollision.GetComponent<LimiterMan>().SetCamera();

            return;



        }*/


    }

    public void DropObject(InputAction.CallbackContext keys)
    {
        if (keys.performed == false)
            return;
        inventoryMan.DropTool();
    }

    public void ToggleInventory()
    {
        Inventory.SetActive(!Inventory.activeSelf);
    }

    
    
}
