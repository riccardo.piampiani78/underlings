using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class FadeInSprite : MonoBehaviour
{
    public float fadeInDuration = 6.0f; 
    public TextMeshProUGUI textMeshProUGUI;
    private float timeElapsed = 6.0f;
    private float alphaValue = 1;
    private bool fadeIn;
    public void SetText(string text)
    {
        
        textMeshProUGUI.text = text;
        fadeIn = true;


    }

    private void Start()
    {
        textMeshProUGUI.color = new Color(textMeshProUGUI.color.r, textMeshProUGUI.color.g, textMeshProUGUI.color.b, 0); // Imposta l'alpha del colore a 0 per renderlo trasparente all'inizio
    }

    private void Fade()
    {
        if (timeElapsed < fadeInDuration)
        {
            
            timeElapsed += Time.deltaTime;
            float alpha = Mathf.Lerp(0, alphaValue, timeElapsed / fadeInDuration); // Calcola l'alpha desiderato con lerp
            textMeshProUGUI.color = new Color(textMeshProUGUI.color.r, textMeshProUGUI.color.g, textMeshProUGUI.color.b, alpha); // Imposta il colore con l'alpha calcolato
        }
    }

    public void Update()
    {
        if (fadeIn)
        {
            timeElapsed = 0;
            fadeIn = false;
        }

        Fade();
    }
}





