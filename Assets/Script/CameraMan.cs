using Assets.Script.Scriptable_Object;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraMan : MonoBehaviour
{
    public Transform player;
    private float X;
    private float Y;
    
    
    private RoomLimiter ActualRoom;

   
    // Update is called once per frame
    void Update()
    {
               
        
        if (player.position.x < ActualRoom.xMax && player.position.x > ActualRoom.xMin)
            X = player.position.x;



        if (player.position.y< ActualRoom.yMax && player.position.y > ActualRoom.yMin)

            Y = player.position.y;

        transform.position = new Vector3(X, Y, -10); 

    }
    public void setActualRoom(RoomLimiter room) {
        ActualRoom = room;

    }
}
